package lab9.event;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A class representing an event and its properties
 * @author Laksono Bramantio
 */
public class Event implements Comparable<Event> {

    /** Name of event */
    private String name;

    /** The time of start time and end time */
    private Calendar startTime;
    private Calendar endTime;

    /** Cost of event */
    private BigInteger cost;

    /**
     * Constructor
     * Initializes a event object with name, startTime, endTime, and cost
     */
    public Event(String name, String startTime, String endTime, String cost) {
        this.name = name;
        this.startTime = parseCalendar(startTime);
        this.endTime = parseCalendar(endTime);
        this.cost = new BigInteger(cost);
    }

    /**
     * Accessor for name field.
     * @return name of this event instance
     */
    public String getName() {
        return this.name;
    }

    /**
     * Accessor for startTime field.
     * @return startTime of this event instance
     */
    public Calendar getStartTime() {
        return startTime;
    }

    /**
     * Accessor for endTime field.
     * @return endTime of this event instance
     */
    public Calendar getEndTime() {
        return endTime;
    }

    /**
     * Accessor for cost field.
     * @return cost of this event instance
     */
    public BigInteger getCost() {
        return cost;
    }

    /**
     * Method untuk mengonversi String waktu ke objek Calender
     * @return objek Calender dari String input
     */
    private Calendar parseCalendar(String date) {
        Calendar newDate = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        try {
            newDate.setTime(sdf.parse(date));
            return newDate;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Method untuk mengecek apakah suatu event
     * overlaps dengan event lain
     *
     * @return true jika overlaps, false jika tidak
     */
    public boolean overlapsWith(Event other){
//        if ((this.getStartTime().compareTo(other.getStartTime()) >= 0)
//                && (this.getEndTime().compareTo(other.getStartTime()) <= 0)  ){
//            return false;
//        }
        if (this.getStartTime().after(other.getEndTime())){
            return false;
        } else if (this.getEndTime().before(other.getStartTime())){
            return false;
        }
        return true;
    }

    /**
     * Method untuk menampilkan info dari
     * suatu event
     *
     * @return String info tentang event tersebut
     */
    public String toString() {
        String text = "";
        SimpleDateFormat sdfOut = new SimpleDateFormat("dd-MM-yyyy, HH:mm:ss");

        text += name + "\n"
                + "Waktu mulai: " + sdfOut.format(startTime.getTime()) + "\n"
                + "Waktu selesai: " + sdfOut.format(endTime.getTime()) + "\n"
                + "Biaya kehadiran: " + cost;
        return text;
    }

    @Override
    public int compareTo(Event e) {
        return startTime.compareTo(e.startTime);
    }
}
