package lab9;

import lab9.user.User;
import lab9.event.Event;
import java.util.ArrayList;

/**
 * Class representing event managing system
 * @author Laksono Bramantio
 */
public class EventSystem {

    /** List of events */
    private ArrayList<Event> events;

    /** List of users */
    private ArrayList<User> users;

    /**
     * Constructor
     * Initializes events and users with empty lists.
     */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * Method untuk mengkases suatu user pada list users
     * @param name String
     * @return user yang dicari
     */
    public User getUser(String name) {
        for (User user : users) {
            if (user.getName().equals(name)) {
                return user;
            }
        }
        return null;
    }

    /**
     * Method untuk mengkases suatu event pada list events
     * @param eventName String
     * @return event yang dicari
     */
    public Event findEvent(String eventName) {
        for (Event event : events) {
            if (event.getName().equals(eventName)) {
                return event;
            }
        }
        return null;
    }

    /**
     * Method untuk mengembalikan String info dari
     * suatu event
     *
     * @param eventName String
     * @return String info suatu event
     */
    public String getEvent(String eventName) {
        Event event = findEvent(eventName);
        return event.toString();
    }

    /**
     * Method untuk menambahkan event ke system
     *
     * @param name
     * @param startTimeStr
     * @param endTimeStr
     * @param costPerHourStr
     * @return String sesuai dengan kondisi yang terjadi
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {
        Event event = findEvent(name);

        if (event != null) {
            return "Event " + name + " sudah ada!";
        } else {
            event = new Event(name, startTimeStr, endTimeStr, costPerHourStr);
            if (event.getStartTime().before(event.getEndTime())) {
                events.add(event);
                return "Event " + name + " berhasil ditambahkan!";
            } else {
                return "Waktu yang diinputkan tidak valid!";
            }
        }
    }

    /**
     * Method untuk menambahkan user ke system
     *
     * @param name
     * @return String sesuai dengan kondisi yang terjadi
     */
    public String addUser(String name) {
        boolean isAvailable = false;
        for (User user : users) {
            if (user.getName().equals(name)) {
                isAvailable = true;
                break;
            }
        }

        if (isAvailable) {
            return "User " + name + " sudah ada!";
        } else {
            User newUser = new User(name);
            users.add(newUser);
            return "User " + name + " berhasil ditambahkan!";
        }
    }

    /**
     * Method untuk mendaftarkan event ke user
     * @param userName
     * @param eventName
     * @return String sesuai dengan kondisi yang terjadi
     */
    public String registerToEvent(String userName, String eventName) {
        User user = getUser(userName);
        Event event = findEvent(eventName);

        if (user == null && event == null) {
            return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        } else if (user == null) {
            return "Tidak ada pengguna dengan nama " + userName + "!";
        } else if (event == null) {
            return "Tidak ada acara dengan nama " + eventName + "!";
        }

        boolean isAdded = user.addEvent(event);
        if (isAdded) {
            return userName + " berencana menghadiri " + eventName + "!";
        } else {
            return userName + " sibuk sehingga tidak dapat menghadiri "
                    + eventName + "!";
        }
    }
}