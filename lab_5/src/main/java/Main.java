import java.util.Scanner;

public class Main {

	public static void main(String[] args) {	
		Scanner input = new Scanner(System.in);

		// Array untuk permainan Bingo
		Number[][] numbers = new Number[5][5];		// array 2D 5x5
		Number[] numberStates = new Number[100];	// array 100

		Number numb;
		int temp;

		for (int i=0; i<5; i++) {
			for (int j=0; j<5; j++) {
				temp = input.nextInt();
				numb = new Number(temp, i, j);

				numbers[i][j] = numb;
				numberStates[temp] = numb;
			}
		}
		input.nextLine();


		// Inisiasi ke Objek BingoCard
		BingoCard permainan = new BingoCard(numbers, numberStates);
		
		// Memulai permainan
		while (true) {
			String inputan = input.nextLine();
			String keyword = inputan.split(" ")[0];

			if (keyword.toUpperCase().equals("INFO")) { // INFO
				System.out.println(permainan.info());
			}
			else if (keyword.toUpperCase().equals("MARK")) { // MARK
				System.out.println(permainan.markNum(Integer.parseInt(inputan.split(" ")[1])));
			}
			else if (keyword.toUpperCase().equals("RESTART")) { // RESTART
				permainan.restart();
			}
			else { // SALAH KEYWORD
				System.out.println("Incorrect command");
			}

			// Cek Menang Bingo atau belum
			if (permainan.isBingo() == true) {
				System.out.println("BINGO!");
				System.out.println(permainan.info());
				System.exit(1);
			}
		}
	}
}