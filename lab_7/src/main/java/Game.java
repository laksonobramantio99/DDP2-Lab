// Laksono Bramantio
// 1706984650

import character.Player;
import character.Human;
import character.Monster;
import character.Magician;

import java.util.ArrayList;

public class Game {
    ArrayList<Player> player = new ArrayList<>();

    /**
     * Fungsi untuk mencari karakter
     * param String name nama karakter yang ingin dicari
     *
     * @return Player chara object karakter yang dicari, return null apabila tidak ditemukan
     */
    public Player find(String name) {
        for (Player character : player) {
            if (character.getName().equals(name)) {
                return character; //jika ditemukan
            }
        }
        return null; //jika tidak ditemukan
    }

    /**
     * fungsi untuk menambahkan karakter ke dalam game
     * param String chara nama karakter yang ingin ditambahkan
     * param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * param int hp hp dari karakter yang ingin ditambahkan
     *
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp) {
        Player character = find(chara);

        if (character != null) { //jika karakter sudah ada
            return "Sudah ada karakter bernama " + chara;
        } else { //jika karakter belum ada
            if (tipe.equals("Human")) {
                Human newHuman = new Human(chara, hp);
                player.add(newHuman);
            } else if (tipe.equals("Monster")) {
                Monster newMonster = new Monster(chara, hp);
                player.add(newMonster);
            } else {
                Magician newMagician = new Magician(chara, hp);
                player.add(newMagician);
            }
            return chara + " ditambah ke game";
        }
    }

    /**
     * fungsi untuk menambahkan karakter dengan tambahan teriakan roar, roar hanya bisa dilakukan oleh monster
     * param String chara nama karakter yang ingin ditambahkan
     * param String tipe tipe dari karakter yang ingin ditambahkan terdiri dari monster, magician dan human
     * param int hp hp dari karakter yang ingin ditambahkan
     * param String roar teriakan dari karakter
     *
     * @return String result hasil keluaran dari penambahan karakter contoh "Sudah ada karakter bernama chara" atau "chara ditambah ke game"
     */
    public String add(String chara, String tipe, int hp, String roar) {
        Player character = find(chara);

        if (character != null) { //jika karakter sudah ada
            return "Sudah ada karakter bernama " + chara;
        } else { //jika karakter belum
            if (tipe.equals("Human")) {
                Human newHuman = new Human(chara, hp);
                player.add(newHuman);
            } else if (tipe.equals("Monster")) {
                Monster newMonster = new Monster(chara, hp, roar);
                player.add(newMonster);
            } else {
                Magician newMagician = new Magician(chara, hp);
                player.add(newMagician);
            }
            return chara + " ditambah ke game";
        }
    }

    /**
     * fungsi untuk menghapus character dari game
     * param String chara character yang ingin dihapus
     *
     * @return String result hasil keluaran dari game
     */
    public String remove(String chara) {
        for (int i = 0; i < player.size(); i++) {
            if (player.get(i).getName().equals(chara)) { //jika ditemukan
                player.remove(i);
                return chara + " dihapus dari game";
            }
        }
        return "Tidak ada " + chara; //jika tidak ditemukan
    }

    /**
     * fungsi untuk menampilkan status character dari game
     * param String chara character yang ingin ditampilkan statusnya
     *
     * @return String result hasil keluaran dari game
     */
    public String status(String chara) {
        Player me = find(chara);
        String text = "";

        if (me == null) { //jika player tidak ditemukan
            return "Tidak ada " + chara;
        } else {
            //baris ke-1
            if (me instanceof Monster) {
                text += "Monster " + chara + "\n";
            } else if (me instanceof Magician) {
                text += "Magician " + chara + "\n";
            } else {
                text += "Human " + chara + "\n";
            }

            //baris ke-2
            text += "HP: " + me.getHp() + "\n";

            //baris ke-3
            if (me.isDead()) {
                text += "Sudah meninggal dunia dengan damai\n";
            } else {
                text += "Masih hidup\n";
            }

            //baris ke-4
            if (me.getDiet().size() == 0) {
                text += "Belum memakan siapa siapa";
            } else {
                text += "Memakan " + diet(chara);
            }
            return text;
        }
    }

    /**
     * fungsi untuk menampilkan semua status dari character yang berada di dalam game
     *
     * @return String result nama dari semua character, format sesuai dengan deskripsi soal atau contoh output
     */
    public String status() {
        if (player.size() == 0) { //jika belum ada player
            return "Tidak ada pemain";
        } else {
            String text = "";
            for (int i = 0; i < player.size(); i++) {
                text += status(player.get(i).getName());
                if (i != player.size() - 1) {
                    text += "\n";
                }
            }
            return text;
        }
    }

    /**
     * fungsi untuk menampilkan character-character yang dimakan oleh chara
     * param String chara Player yang ingin ditampilkan seluruh history player yang dimakan
     *
     * @return String result hasil dari karakter yang dimakan oleh chara
     */
    public String diet(String chara) {
        Player character = find(chara);

        if (character == null) { //jika tidak ditemukan
            return "Tidak ada " + chara;
        } else {
            if (character.getDiet().size() == 0) { //jika belum pernah makan
                return "Belum ada yang termakan";
            } else {
                String diet = "";
                for (int i = 0; i < character.getDiet().size(); i++) {
                    diet += character.getDiet().get(i);

                    if (i != character.getDiet().size() - 1) {
                        diet += ", ";
                    }
                }
                return diet;
            }
        }
    }

    /**
     * fungsi helper untuk memberikan list character yang dimakan dalam satu game
     *
     * @return String result hasil dari karakter yang dimakan dalam 1 game
     */
    public String diet() {
        String text = "";

        for (Player character : player) { //loop tiap player
            for (String name : character.getDiet()) {
                text += name + ", ";
            }
        }

        if (text.equals("")) { //jika belum ada yang termakan
            return "Belum ada yang termakan";
        } else {
            return "Termakan : " + text;
        }
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName
     * param String meName nama dari character yang sedang dimainkan
     * param String enemyName nama dari character yang akan di serang
     * return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String attack(String meName, String enemyName) {
        Player me = find(meName);
        Player enemy = find(enemyName);

        if (me == null || enemy == null) { //jika salah satu/kedua player tidak ada
            return "Tidak ada " + meName + " atau " + enemyName;
        } else if (me.isDead()) { //jika me sudah mati
            return meName + " tidak bisa menyerang " + enemyName;
        } else {
            me.attack(enemy);
            return "Nyawa " + enemyName + " " + enemy.getHp();
        }
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. Method ini hanya boleh dilakukan oleh magician
     * param String meName nama dari character yang sedang dimainkan
     * param String enemyName nama dari character yang akan di bakar
     *
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String burn(String meName, String enemyName) {
        Player me = find(meName);
        Player enemy = find(enemyName);

        if (me == null || enemy == null) { //jika salah satu/kedua player tidak ada
            return "Tidak ada " + meName + " atau " + enemyName;
        } else if (me instanceof Magician) {
            if (me.isDead()) { //jika magician telah mati
                return meName + " tidak bisa membakar " + enemyName;
            } else {
                ((Magician) me).burn(enemy);
                if (enemy.isBurned()) {
                    return "Nyawa " + enemyName + " 0 \n dan matang";
                } else {
                    return "Nyawa " + enemyName + " " + enemy.getHp();
                }
            }
        } else { // bukan magician
            return meName + " tidak bisa melakukan burn";
        }
    }

    /**
     * fungsi untuk menampilkan hasil dari me vs enemyName. enemy hanya bisa dimakan sesuai dengan deskripsi yang ada di soal
     * param String meName nama dari character yang sedang dimainkan
     * param String enemyName nama dari character yang akan di makan
     *
     * @return String result kembalian dari me Vs enemy, format sesuai deskripsi soal
     */
    public String eat(String meName, String enemyName) {
        Player me = find(meName);
        Player enemy = find(enemyName);

        if (me == null || enemy == null) { //jika salah satu/kedua player tidak ada
            return "Tidak ada " + meName + " atau " + enemyName;
        } else {
            if (me.isDead()) { //jika me sudah mati
                return meName + " tidak bisa memakan " + enemyName;
            } else if (!(me.canEat(enemy))) { //tidak bisa memakan
                return meName + " tidak bisa memakan " + enemyName;
            } else { //bisa memakan
                me.setHp(me.getHp() + 15); //tambah darah
                remove(enemyName); //remove yang dimakan

                if (enemy instanceof Monster) {
                    me.getDiet().add("Monster " + enemyName);
                } else if (enemy instanceof Magician) {
                    me.getDiet().add("Magician " + enemyName);
                } else {
                    me.getDiet().add("Human " + enemyName);
                }

                return meName + " memakan " + enemyName + "\nNyawa " + meName + " kini " + me.getHp();
            }
        }
    }

    /**
     * fungsi untuk berteriak. Hanya dapat dilakukan oleh monster.
     * param String meName nama dari character yang akan berteriak
     *
     * @return String result kembalian dari teriakan monster, format sesuai deskripsi soal
     */
    public String roar(String meName) {
        Player me = find(meName);

        if (me == null) { // jika not found
            return "Tidak ada " + meName;
        } else if (me instanceof Monster) { // jika monster
            return ((Monster) me).roar();
        } else { // jika bukan monster
            return meName + " tidak bisa berteriak";
        }
    }
}