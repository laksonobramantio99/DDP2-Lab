// Laksono Bramantio
// 1706984650

package character;

public class Monster extends Player {
    private String roar;

    public Monster(String name, int hp) {
        super(name, 2 * hp);
        this.roar = "AAAAAAaaaAAAAAaaaAAAAAA";
    }

    public Monster(String name, int hp, String roar) {
        super(name, 2 * hp);
        this.roar = roar;
    }

    public String roar() { //monster roar
        return roar;
    }
}