// Laksono Bramantio
// 1706984650

package character;

public class Magician extends Human {

    public Magician(String name, int hp) {
        super(name, hp);
    }

    public void burn(Player enemy) {
        //mengurangi darah
        this.attack(enemy);

        if (enemy.isDead()) { // menjadikan burned
            enemy.setBurned(true);
        }
    }
}