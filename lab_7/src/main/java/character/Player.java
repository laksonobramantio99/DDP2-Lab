// Laksono Bramantio
// 1706984650

package character;

import java.util.ArrayList;

public class Player {
    private String name;
    private int hp;
    private ArrayList<String> diet;
    private boolean isBurned;

    public Player(String name, int hp) {
        this.name = name;
        this.hp = hp;
        this.diet = new ArrayList<>();
        this.isBurned = false;
    }

    public String getName() {
        return name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public ArrayList<String> getDiet() {
        return diet;
    }

    public boolean isBurned() {
        return isBurned;
    }

    public void setBurned(boolean burned) {
        isBurned = burned;
    }

    public boolean isDead() { //untuk mengetahui sudah mati atau belum
        if (hp == 0) {
            return true;
        }
        return false;
    }

    public boolean canEat(Player enemy) {
        if (this instanceof Human) { //pemakan adalah HUMAN
            if (enemy instanceof Human) { //jika makan HUMAN
                return false;
            } else { //jika makan MONSTER
                if (enemy.isBurned()) {
                    return true;
                } else {
                    return false;
                }
            }
        } else { //pemakan adalah MONSTER
            if (enemy.isDead()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public void attack(Player enemy) {
        //mengurangi darah
        if (enemy instanceof Magician) { //jika attack Magician
            enemy.setHp(enemy.getHp() - 20);
        } else {
            enemy.setHp(enemy.getHp() - 10);
        }

        if (enemy.getHp() < 0) { //menjaga darah agar tidak < 0
            enemy.setHp(0);
        }
    }
}