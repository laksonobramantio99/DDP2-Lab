// Laksono Bramantio
// 1706984650

public class Manusia {
   
    private String nama;
    private int umur;
    private int uang;
    private float kebahagiaan;
    
    // Constructor 
    public Manusia(String nama, int umur) {
        this.nama = nama;
        this.umur = umur;
        this.uang = 50000;
        this.kebahagiaan = 50;
    }
    // Constructor
    public Manusia(String nama, int umur, int uang) {
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;
        this.kebahagiaan = 50;
    }
    
    // Setter & Getter Nama
    public String getNama() {
        return this.nama;
    }
    public void setNama(String nama) {
        this.nama = nama;
    }
    
    // Setter & Getter Umur
    public int getUmur() {
        return this.umur;
    }
    public void setUmur (int umur) {
        this.umur = umur;
    }
    
    // Setter & Getter Uang
    public int getUang() {
        return this.uang;
    }
    public void setUang(int uang) {
        this.uang = uang;
    }
    
    // Setter & Getter Kebahagiaan
    public float getKebahagiaan() {
        return this.kebahagiaan;
    }
    public void setKebahagiaan(float kebahagiaan) {
        this.kebahagiaan = kebahagiaan;
    }

    // Method Beri Uang 1 parameter
    public void beriUang(Manusia penerima) {
        int uangTampung = 0;
        for (int i=0; i<penerima.getNama().length(); i++) {
            int ascii = (int) (penerima.getNama().charAt(i));
            uangTampung += ascii;
        }
        int jumlahUang = uangTampung * 100;
        
        if (jumlahUang > this.getUang()) {
            System.out.println(this.getNama() + " ingin memberi uang kepada " +
                    penerima.getNama() + " namun tidak memiliki cukup uang :'(");
        }
        
        else {
            this.setUang(this.getUang()-jumlahUang);
            penerima.setUang(penerima.getUang()+jumlahUang);
            
            float tambahKebahagiaan = (float) jumlahUang / 6000;
            // sisi pengirim
            if (this.getKebahagiaan() + tambahKebahagiaan > 100) {
                this.setKebahagiaan(100);
            }
            else {
                this.setKebahagiaan(this.getKebahagiaan() + tambahKebahagiaan);
            }
            // sisi penerima
            if (penerima.getKebahagiaan() + tambahKebahagiaan > 100) {
                penerima.setKebahagiaan(100);
            }
            else {
                penerima.setKebahagiaan(penerima.getKebahagiaan() + tambahKebahagiaan);
            }
            System.out.println(this.getNama()+" memberi uang sebanyak "+ jumlahUang +
                    " kepada "+ penerima.getNama()+ ", mereka berdua senang :D");  
        }
    }

    // Method Beri Uang 2 parameter
    public void beriUang(Manusia penerima, int jumlahUang) {
        if (jumlahUang > this.getUang()) {
            System.out.println(this.getNama() + " ingin memberi uang kepada " +
                    penerima.getNama() + " namun tidak memiliki cukup uang :'(");
        }
        
        else {
            this.setUang(this.getUang()-jumlahUang);
            penerima.setUang(penerima.getUang()+jumlahUang);
            
            float tambahKebahagiaan = (float) jumlahUang / 6000;
            // sisi pengirim
            if (this.getKebahagiaan() + tambahKebahagiaan > 100) {
                this.setKebahagiaan(100);
            }
            else {
                this.setKebahagiaan(this.getKebahagiaan() + tambahKebahagiaan);
            }
            // sisi penerima
            if (penerima.getKebahagiaan() + tambahKebahagiaan > 100) {
                penerima.setKebahagiaan(100);
            }
            else {
                penerima.setKebahagiaan(penerima.getKebahagiaan() + tambahKebahagiaan);
            }
            System.out.println(this.getNama()+" memberi uang sebanyak "+ jumlahUang +
                    " kepada "+ penerima.getNama()+ ", mereka berdua senang :D");  
        }
    }
    
    // Method Bekerja
    public void bekerja(int durasi, int bebanKerja) {
        if (this.getUmur() < 18) {
            System.out.println(this.getNama() +
                    " belum boleh bekerja karena masih dibawah umur D:");
        }
        
        else {
            float bebanKerjaTotal = (float) durasi * bebanKerja;
            
            if (bebanKerjaTotal <= this.getKebahagiaan()) {
                this.setKebahagiaan(this.getKebahagiaan() - bebanKerjaTotal);
                this.setUang(this.getUang() + ((int)bebanKerjaTotal*10000));
                System.out.println(this.getNama()+" bekerja full time, total pendapatan : "+
                        ((int)bebanKerjaTotal*10000));
            }
            
            else {
                int durasiBaru = (int) (this.getKebahagiaan() / bebanKerja);
                bebanKerjaTotal = (float) durasiBaru * bebanKerja;
                int pendapatan = (int) bebanKerjaTotal * 10000;
                this.setUang(this.getUang()+pendapatan);
                this.setKebahagiaan(this.getKebahagiaan()-bebanKerjaTotal);
                System.out.println(this.getNama()+
                        " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : "+
                        pendapatan);
            }
        }
    }
    
    public void rekreasi(String tempat) {
        int biaya = tempat.length() * 10000;
        
        if (this.getUang()>=biaya) {
            this.setUang(this.getUang()-biaya);
            this.setKebahagiaan(this.getKebahagiaan()+tempat.length());
            if (this.getKebahagiaan()>100) {
                this.setKebahagiaan(100);
            }
            System.out.println(this.getNama()+
                    " berekreasi di "+ tempat + ", " + this.getNama()+
                    " senang :)");
        }
        else {
            System.out.println(this.getNama()+" tidak mempunyai cukup uang untuk berekreasi di "+
                    tempat + " :(");
        }
        
    }

    public void sakit(String namaPenyakit) {
        this.setKebahagiaan(this.getKebahagiaan()-namaPenyakit.length());
        if (this.getKebahagiaan() < 0) {
            this.setKebahagiaan(0);
        }
        System.out.println(this.getNama()+" terkena penyakit "+
                namaPenyakit + " :O");
    }
    
    public String toString() {
        String teks = "Nama       : " + this.getNama() + "\n" +
                      "Umur       : " + this.getUmur() + "\n" +
                      "Uang       : " + this.getUang() + "\n" +
                      "Kebahagiaan: " + this.getKebahagiaan() ;
        return teks;
    }
}
