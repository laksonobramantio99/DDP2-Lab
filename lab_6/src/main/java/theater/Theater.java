package theater;

import java.util.ArrayList;
import customer.*;
import movie.*;
import ticket.*;


public class Theater {
    
    private String nama;
    private int saldo;
    private ArrayList<Ticket> arrListTicket;
    private Movie[] movies;
    
    public Theater(String nama, int saldo, ArrayList<Ticket> arrListTicket, Movie[] movies) {
        this.nama = nama;
        this.saldo = saldo;
        this.arrListTicket = arrListTicket;
        this.movies = movies;
    }
    
    public String getNama() {
        return nama;
    }
    
    public int getSaldo() {
        return saldo;
    }
    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }
    
    public ArrayList<Ticket> getArrListTicket() {
        return arrListTicket;
    }
    
    public Movie[] getMovies() {
        return movies;
    }
    
    
    public void printInfo() {
        String daftarFilm = "";
        for (int i=0; i<movies.length; i++) {
            daftarFilm += movies[i].getJudul();
            if (i != ((movies.length)-1)){
                daftarFilm += ", ";
            }
        }
        System.out.println(
                "------------------------------------------------------------------\n"+
                "Bioskop                 : " + nama + "\n"+
                "Saldo Kas               : " + saldo + "\n"+
                "Jumlah tiket tersedia   : " + arrListTicket.size() + "\n"+
                "Daftar Film tersedia    : " + daftarFilm + "\n"+
                "------------------------------------------------------------------");
    }
    
    public static void printTotalRevenueEarned(Theater[] theaters) {
        int totalUang = 0;
        
        for (Theater bioskop : theaters) {
            totalUang += bioskop.getSaldo();
        }
        System.out.println("Total uang yang dimiliki Koh Mas : Rp. "+ totalUang);
        System.out.println("------------------------------------------------------------------");
        
        for (Theater bioskop : theaters) {
            System.out.println("Bioskop     : "+ bioskop.getNama());
            System.out.println("Saldo Kas   : Rp. "+ bioskop.getSaldo());
            System.out.println();
        }
        System.out.println("------------------------------------------------------------------");
    }
}