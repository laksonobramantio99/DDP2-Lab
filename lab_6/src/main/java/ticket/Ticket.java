package ticket;
import customer.*;
import movie.*;
import theater.*;

public class Ticket {
    
    private Movie film;
    private String jadwal;
    private boolean jenis;
    private String ticketID;
    
    public Ticket(Movie film, String jadwal, boolean jenis) {
        this.film = film;
        this.jadwal = jadwal;
        this.jenis = jenis;
        this.ticketID = film.getJudul() + jadwal + this.getJenisFilm();
    }
    
    public Movie getFilm() {
        return film;
    }
    
    public String getJenisFilm() {
        if (jenis) return "3 Dimensi";
        else return "Biasa";
    }
    
    public String getID() {
        return ticketID;
    }
    
    
}