package movie;

import customer.*;
import theater.*;
import ticket.*;

public class Movie {
    
    private String judul;
    private String rating;
    private int durasi;
    private String genre;
    private String jenis;
    
    public Movie(String judul, String rating, int durasi, String genre, String jenis) {
        this.judul = judul;
        this.rating = rating;
        this.durasi = durasi;
        this.genre = genre;
        this.jenis = jenis;
    }
    
    public String getJudul() {
        return judul;
    }
    public String getRating() {
        return rating;
    }
    public int getDurasi() {
        return durasi;
    }
    public String getGenre() {
        return genre;
    }
    public String getJenis() {
        return jenis;
    }
}