package customer;

import movie.*;
import theater.*;
import ticket.*;

public class Customer {
    
    private String nama;
    private String gender;
    private int umur;
    
    public Customer(String nama, String gender, int umur) {
        this.nama = nama;
        this.gender = gender;
        this.umur = umur;
    }
    
    public boolean cekBelumCukupUmur(int umur, String rating) {
        int batasUmur = 0;
        if (rating.equals("Remaja")){
            batasUmur = 13;
        } else if (rating.equals("Dewasa")) {
            batasUmur = 17;
        }
        return batasUmur >= umur;
    }
    
    
    
    public Ticket orderTicket(Theater bioskop, String judul, String jadwal, String jenis) {
        String id = judul + jadwal + jenis;
        boolean ticketExist = false;
        
        for (Ticket aTicket : bioskop.getArrListTicket()) {
            
            if (id.equals(aTicket.getID())) {
                ticketExist = true;
                
                if (cekBelumCukupUmur(this.umur , aTicket.getFilm().getRating())) {
                    System.out.println(nama+" masih belum cukup umur untuk menonton "
                            + judul +" dengan rating "
                            + aTicket.getFilm().getRating());
                    return null;
                }
                
                int hargaTiket = 60000;
                if (jadwal.equals("Sabtu") || jadwal.equals("Minggu")) {
                    hargaTiket += 40000;
                }
                
                if (jenis.equals("3 Dimensi")) {
                    hargaTiket = (int) (1.2 * hargaTiket);
                }
                
                bioskop.setSaldo(bioskop.getSaldo() + hargaTiket);
                
                System.out.println(this.nama+" telah membeli tiket "+ judul+
                        " jenis "+ jenis+ " di "+ bioskop.getNama() + " pada hari "+ jadwal +" seharga Rp. "+ hargaTiket);
                
                return aTicket;    
            }
        }
        if (!ticketExist) {
            System.out.println("Tiket untuk film "+ judul +" jenis "+jenis
                    +" dengan jadwal " + jadwal + " tidak tersedia di "+ bioskop.getNama());
        }
        return null;
    }
    
    
    public void findMovie(Theater bioskop, String judul) {
        boolean filmExists = false;
        for (Movie movie : bioskop.getMovies()) { // untuk setiap movie yang ada di daftar film maka,
            if (movie.getJudul().equals(judul)){ //kalau judulnya sama yaudah di break, ga nyari lagi
                filmExists = true;
                System.out.println("------------------------------------------------------------------");
                System.out.println("Judul   : "+ movie.getJudul());
                System.out.println("Genre   : "+ movie.getGenre());
                System.out.println("Durasi  : "+ movie.getDurasi() + " menit");
                System.out.println("Rating  : "+ movie.getRating());
                System.out.println("Jenis   : Film "+ movie.getJenis());
                System.out.println("------------------------------------------------------------------");
                break;
            }
        }
        if (!filmExists){
            System.out.println("Film " +judul+ " yang dicari " +nama+ " tidak ada di bioskop " +bioskop.getNama());

        }
    }
}