/*
Laksono Bramantio
1706984650
*/

import java.util.Scanner;

public class Lab8 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        Korporasi ptTampan = new Korporasi(); //Membuat perusahaan PT Tampan

        try {

            System.out.print("Batas gaji: ");
            Korporasi.batasGajiStaff = Integer.parseInt(input.nextLine()); // input batas gaji staff

            while (true) {
                String inputan = input.nextLine();
                String keyword = inputan.split(" ")[0];

                if (keyword.equals("TAMBAH_KARYAWAN")) {
                    String nama = inputan.split(" ")[1];
                    String tipe = inputan.split(" ")[2];
                    int gaji = Integer.parseInt(inputan.split(" ")[3]);

                    System.out.println(ptTampan.tambahKaryawan(nama, tipe, gaji));
                } else if (keyword.equals("STATUS")) {
                    String nama = inputan.split(" ")[1];

                    System.out.println(ptTampan.statusKaryawan(nama));
                } else if (keyword.equals("TAMBAH_BAWAHAN")) {
                    String namaPerekrut = inputan.split(" ")[1];
                    String namaCalon = inputan.split(" ")[2];

                    System.out.println(ptTampan.perekrutanBawahan(namaPerekrut, namaCalon));
                } else if (keyword.equals("GAJIAN")) {
                    System.out.println(ptTampan.gajian());
                } else if (keyword.equals("EXIT")) {
                    System.out.println("BYE..");
                    break;
                } else {
                    System.out.println("Input salah!");
                }
            }
        } catch (Exception e) { // Exception
            System.out.println("Invalid input!");
            System.out.println("===================================");
        }
    }
}
