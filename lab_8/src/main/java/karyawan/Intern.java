/*
Laksono Bramantio
1706984650
*/

package karyawan;

public class Intern extends Karyawan {

    public Intern(String nama, int gaji) {
        super(nama, gaji);
    }

    @Override
    public boolean dapatMemilikiBawahan(Karyawan calonBawahan) {
        return false;
    }
}
