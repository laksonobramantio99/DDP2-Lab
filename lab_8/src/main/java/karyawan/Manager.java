/*
Laksono Bramantio
1706984650
*/

package karyawan;

public class Manager extends Karyawan {

    public Manager(String nama, int gaji) {
        super(nama, gaji);
    }

    @Override
    public boolean dapatMemilikiBawahan(Karyawan calonBawahan) {
        if (!(calonBawahan instanceof Manager)) {
            return true;
        }
        return false;
    }
}
