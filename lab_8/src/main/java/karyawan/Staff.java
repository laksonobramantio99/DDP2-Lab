/*
Laksono Bramantio
1706984650
*/

package karyawan;

public class Staff extends Karyawan {

    public Staff(String nama, int gaji) {
        super(nama, gaji);
    }

    @Override
    public boolean dapatMemilikiBawahan(Karyawan calonBawahan) {
        if (calonBawahan instanceof Intern) {
            return true;
        }
        return false;
    }
}