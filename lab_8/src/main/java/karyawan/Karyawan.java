/*
Laksono Bramantio
1706984650
*/

package karyawan;

import java.util.ArrayList;

public abstract class Karyawan {
    protected String nama;
    protected int gaji;
    protected int counterNaikGaji;
    protected ArrayList<Karyawan> daftarBawahan = new ArrayList<>();

    public Karyawan(String nama, int gaji) {
        this.nama = nama;
        this.gaji = gaji;
        this.counterNaikGaji = 0;
    }

    public String getNama() {
        return nama;
    }

    public int getGaji() {
        return gaji;
    }

    public void setGaji(int gaji) {
        this.gaji = gaji;
    }

    public int getCounterNaikGaji() {
        return counterNaikGaji;
    }

    public void setCounterNaikGaji(int counterNaikGaji) {
        this.counterNaikGaji = counterNaikGaji;
    }

    public ArrayList<Karyawan> getDaftarBawahan() {
        return daftarBawahan;
    }

    public void naikGaji() {
        this.setGaji( (int) (1.1 * this.gaji) );
    }

    public abstract boolean dapatMemilikiBawahan(Karyawan calonBawahan);
}
