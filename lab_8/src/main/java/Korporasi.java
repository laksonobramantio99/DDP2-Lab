/*
Laksono Bramantio
1706984650
*/

import karyawan.Intern;
import karyawan.Karyawan;
import karyawan.Manager;
import karyawan.Staff;

import java.util.ArrayList;

public class Korporasi {

    ArrayList<Karyawan> daftarKaryawan = new ArrayList<>();
    static int batasGajiStaff;

    public Karyawan cariKaryawan(String nama) {
        for (Karyawan karyawan : daftarKaryawan) {
            if (karyawan.getNama().equals(nama)) {
                return karyawan; //jika ditemukan
            }
        }
        return null; //jika tidak ditemukan
    }

    public String tambahKaryawan(String nama, String tipe, int gaji) {
        Karyawan employee = cariKaryawan(nama);

        if (employee != null) { //jika karakter sudah ada
            return "Karyawan dengan nama " + nama + " telah terdaftar";
        } else if (daftarKaryawan.size() == 10000) { //jika sudah ada 10000 karywan
            return "Tidak bisa menambah karyawan lagi";
        } else { //jika karakter belum ada
            if (tipe.equals("MANAGER")) {
                Manager newObj = new Manager(nama, gaji);
                daftarKaryawan.add(newObj);
            } else if (tipe.equals("STAFF")) {
                Staff newObj = new Staff(nama, gaji);
                daftarKaryawan.add(newObj);
            } else if (tipe.equals("INTERN")) {
                Intern newObj = new Intern(nama, gaji);
                daftarKaryawan.add(newObj);
            } else {
                return "Tidak ada karyawan dengan tipe " + tipe;
            }
            return (nama + " mulai bekerja sebagai " + tipe.toUpperCase() + " di PT. TAMPAN");
        }
    }

    public String statusKaryawan(String nama) {
        Karyawan employee = cariKaryawan(nama);

        if (employee == null) {
            return "Karyawan tidak ditemukan";
        } else {
            return employee.getNama() + " " + employee.getGaji();
        }
    }

    public String perekrutanBawahan(String namaPerekrut, String namaCalon) {
        Karyawan perekrut = cariKaryawan(namaPerekrut);
        Karyawan calon = cariKaryawan(namaCalon);

        if (perekrut == null || calon == null) { //jika salah satu karyawan tidak ditemukan
            return "Nama tidak berhasil ditemukan";
        } else if (perekrut.getDaftarBawahan().size() == 10) { //sudah memiliki 10 bawahan
            return namaPerekrut + " sudah memiliki 10 bawahan";
        } else if (!perekrut.dapatMemilikiBawahan(calon)) { //tidak memenuhi syarat rekrutmen
            return "Anda tidak layak memiliki bawahan";
        } else if (perekrut.getDaftarBawahan().contains(calon)) { //sudah direkrut
            return "Karyawan " + namaCalon + " telah menjadi bawahan " + namaPerekrut;
        } else { //bisa
            perekrut.getDaftarBawahan().add(calon);
            return "Karyawan " + namaCalon + " berhasil ditambahkan menjadi " +
                    "bawahan " + namaPerekrut;
        }
    }

    public String gajian() {
        String text = "Semua karyawan telah diberikan gaji";
        ArrayList<Karyawan> deleteKaryawan = new ArrayList<>();

        // Menambah 1 pada counterGaji
        for (int i = 0; i < daftarKaryawan.size(); i++) {
            Karyawan employee = daftarKaryawan.get(i);
            employee.setCounterNaikGaji(employee.getCounterNaikGaji() + 1);
        }

        // Cek untuk NAIK GAJI & NAIK JABATAN
        for (int i = 0; i < daftarKaryawan.size(); i++) {
            Karyawan employee = daftarKaryawan.get(i);

            // Naik gaji
            if (employee.getCounterNaikGaji() % 6 == 0 && employee.getCounterNaikGaji() > 0) { //naik gaji
                text += "\n" + employee.getNama() + " mengalami kenaikan gaji sebesar 10% dari " + employee.getGaji() +
                        " menjadi " + ((int) (1.1 * employee.getGaji()));
                employee.naikGaji(); // NAIK GAJI

                // Naik jabatan
                if (employee instanceof Staff && employee.getGaji() > batasGajiStaff) {//jika dia promosi
                    text += "\nSelamat, " + employee.getNama() + " telah dipromosikan menjadi MANAGER";

                    // Naik jabatan menjadi MANAGER
                    Manager newObj = new Manager(employee.getNama(), employee.getGaji());
                    daftarKaryawan.add(newObj);
                    deleteKaryawan.add(employee);
                }
            }
        }

        //menghapus karyawan yg sudah naik jabatan
        for (Karyawan deleteEmployee : deleteKaryawan) {
            daftarKaryawan.remove(deleteEmployee);
        }

        return text;
    }
}
