/*
Nama : Laksono Bramantio
NPM  : 1706984650
DDP2 - B
*/

import java.util.Scanner;
public class RabbitHouse {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String masukkan = input.nextLine();

        String keyword = masukkan.split(" ")[0]; //keyword
        String nama = masukkan.split(" ")[1]; // nama
        int len = nama.length(); //panjang nama

        // Untuk keyword "normal"
        if ("normal".equals(keyword.toLowerCase())) { 
            // Mengaplikasikan fungsi 
            int jumlah = normal(len);
            System.out.println(jumlah);
        }

        // Untuk keyword "palindrome"
        else if ("palindrome".equals(keyword.toLowerCase())) {
            // Mengaplikasikan fungsi
            int jumlah = palindrome(nama);
            System.out.println(jumlah);
        }
        
        input.close();
    }  

    // Method hitung normal
    public static int normal(int len){
        if (len <= 1) {
            return len;
        }
        else {
            return 1 + len * normal(len-1);
        }
    }

    // Method untuk cek apakah palindrome atau tidak
    public static boolean isPalindrome(String str) {
        return str.equals(new StringBuffer(str).reverse().toString());
    }

    // Method hitung untuk yang palindrome
    public static int palindrome(String nama){
        int hasil = 0;
        int len = nama.length();

        if (isPalindrome(nama)) {  //jika nama palindrome
            return 0;
        }
        
        else { //jika bukan palindrom
            hasil += 1;
            for (int i=0; i<len; i++) {
                hasil += palindrome(nama.substring(0,i) + nama.substring(i+1,len));
            }
            return hasil; 
        }
    }
}

