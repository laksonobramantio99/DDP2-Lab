import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Laksono Bramantio):
 * @author Laksono Bramantio, NPM 1706984650 Kelas: DDP2-B GitLab Account: laksono.bramantio
 */

public class SistemSensus {

	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
						"--------------------\n" +
						"Nama Kepala Keluarga   : "); 
		String nama = input.nextLine();

		System.out.print("Alamat Rumah           : ");
		String alamat = input.nextLine();

		System.out.print("Panjang Tubuh (cm)     : ");
		short panjang = Short.parseShort(input.nextLine());
		if (panjang <= 0 || panjang > 250) {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}

		System.out.print("Lebar Tubuh (cm)       : ");
		short lebar = Short.parseShort(input.nextLine());
		if (lebar <= 0 || lebar > 250) {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}

		System.out.print("Tinggi Tubuh (cm)      : ");
		short tinggi = Short.parseShort(input.nextLine());
		if (tinggi <= 0 || tinggi > 250) {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}

		System.out.print("Berat Tubuh (kg)       : ");
		double berat = Double.parseDouble(input.nextLine());
		if (berat <= 0 || berat > 150) {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}

		System.out.print("Jumlah Anggota Keluarga: ");
		byte makanan = Byte.parseByte(input.nextLine());
		if (makanan <= 0 || makanan > 20) {
			System.out.println("WARNING: Keluarga ini tidak perlu direlokasi!");
			System.exit(0);
		}

		System.out.print("Tanggal Lahir          : ");
		String tanggalLahir = input.nextLine();

		System.out.print("Catatan Tambahan       : ");
		String catatan = input.nextLine();

		System.out.print("Jumlah Cetakan Data    : ");
		byte jumlahCetakan = Byte.parseByte(input.nextLine());

		System.out.println();
		
		if (asa) {
			laksoon
		}



		// TODO Periksa ada catatan atau tidak
		if ("".equals(catatan)) {
			catatan = "Tidak ada catatan tambahan";
		}
		else {
			catatan = "Catatan: " + catatan;
		}

		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// TODO Hitung rasio berat per volume (rumus lihat soal)
		short rasio = (short) (berat / (panjang*lebar*tinggi) * 1000000);

		for (int i=1; i <= jumlahCetakan; i++) {
			// TODO Minta masukan terkait nama penerima hasil cetak data
			System.out.print("Pencetakan " + i + " dari " + jumlahCetakan + " untuk: ");
			String penerima = input.nextLine(); // Lakukan baca input lalu langsung jadikan uppercase

			penerima = penerima.toUpperCase();

			// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
			String hasil;
			hasil = "DATA SIAP DICETAK SESUAI UNTUK " + penerima + "\n" +
					"------------------------------\n" +
					nama + " - " + alamat + "\n" +
                    "Lahir pada tanggal " + tanggalLahir + "\n" +
                    "Rasio Berat Per Volume  = " + rasio + "kg/m^3\n" +
                    catatan + "\n";

			System.out.println(hasil);
		}

				
		// SOAL BONUS
		// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)
		char[] karakter = nama.toCharArray();
		short jumlahanAscii = 0;
		for (char x:karakter) {
			jumlahanAscii += (short) x;
		}
		short kalkulasi = (short) (((panjang*lebar*tinggi) + jumlahanAscii) % 10000);

		
		// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = nama.charAt(0) + String.valueOf(kalkulasi);

		
		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = (int) (50000*365*makanan);
		
		
		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		short tahunLahir = Short.parseShort(tanggalLahir.split("-")[2]); // lihat hint jika bingung
		short umur = (short) (2018 - tahunLahir);


		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String namaTempat, namaKabupaten;
		if (umur<=18) {
			namaTempat = "PPMT";
			namaKabupaten = "Rotunda";
		}
		else {
			if (anggaran <= 100000000) {
				namaTempat = "Teksas";
				namaKabupaten = "Sastra";
			}
			else {
				namaTempat = "Mares";
				namaKabupaten = "Margonda";
			}	
		}


		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		String rekomendasi =
		"REKOMENDASI APARTEMEN\n" +
    	"---------------------\n" +
    	"MENGETAHUI: Identitas keluarga: "+nama+" - "+nomorKeluarga+"\n"+
    	"MENIMBANG : Anggaran makanan tahunan: Rp "+anggaran+ "\n"+
		"            Umur kepala keluarga: "+umur+ " tahun\n"+
		"MEMUTUSKAN: Keluarga "+nama+" akan ditempatkan di:\n"+
		namaTempat+", Kabupaten "+ namaKabupaten;

		System.out.println(rekomendasi);


		input.close();
	}
}
